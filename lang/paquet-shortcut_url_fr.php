<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/shortcut_url.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'shortcut_url_description' => 'CE plugin permet de raccourcisseur les URL',
	'shortcut_url_slogan' => 'Raccourcisseur d’URL pour SPIP'
);
