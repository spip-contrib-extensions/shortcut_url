<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/shortcut_url?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_shortcut_title' => 'Add shortcut',
	'ajouter_shortcut_url' => 'Add URL',
	'auteur_shortcut_url' => 'URL created by : ',

	// C
	'config_export_ok' => 'Export of data is completed successfully',

	// E
	'erreur_url_exist' => 'This URL already exists',
	'erreur_url_invalide' => 'Please add valid URL',
	'erreur_url_raccourcis_exist' => 'This shortcut URL already exists',

	// F
	'form_click' => 'Num clicks',
	'form_country_code' => 'Country code',
	'form_date_connect' => 'Connection data',
	'form_date_insert' => 'Date insert',
	'form_date_modif' => 'Date published',
	'form_description' => 'Description',
	'form_edit' => 'Edit',
	'form_id_shortcut_urls' => 'id',
	'form_ip_address' => 'IP Address',
	'form_maj' => 'MAJ',
	'form_nom_pays' => 'Country ISO code',
	'form_referrer' => 'Referer',
	'form_titre' => 'Title',
	'form_url' => 'URL',
	'form_user_agent' => 'User agent',

	// I
	'icone_stats_shortcut' => 'Statistics',
	'icone_stats_shortcut_url' => 'Return to the statistics list',
	'icone_supprimer_shortcut_url' => 'Delete shortcut',
	'info_1_shortcut_url' => '@nb@ URL shortcut',
	'info_1_shortcut_url_bot' => 'Bot click',
	'info_1_shortcut_url_humain' => 'Human click',
	'info_nb_shortcut_url_clicks' => 'Total click',
	'info_nb_shortcut_url_stat' => '@nb@ connection on the set of URLs',
	'info_nb_shortcut_url_stats' => '@nb@ connections on the set of URLs',
	'info_nb_shortcut_urls' => '@nb@ URL shortcuts',
	'item_utiliser_shortcut_export' => 'Export Shortcut statistics',
	'item_utiliser_shortcut_title' => 'You can défine shortcut URL (max @nb@ characters)',
	'item_utiliser_shortcut_url' => 'Add URL',

	// L
	'label_annee' => 'Select year',
	'label_autres' => 'Others',
	'label_mois' => 'Select month',

	// M
	'message_confirmation_shortcut_url' => 'Your shortcut was recorded.',

	// N
	'nb_click' => 'clicks',
	'non_communique' => 'nc',

	// P
	'partage_facebook' => 'Share on Facebook',
	'partage_googleplus' => 'Share on GooglePlus',
	'partage_seenthis' => 'Share on Seenthis',
	'partage_twitter' => 'Share on Twitter',
	'pas_de_shortcut_url' => 'Not URL shortcut',
	'pas_de_statistique' => 'No statistics',
	'plugin_d3js_noninstalle' => 'D3js plugin is not installed',

	// S
	'shortcut_url' => 'Shortcut URL',
	'shortcut_url_logs' => 'Shortcut stats',
	'shortcut_url_logs_export' => 'Export shortcuts',
	'supprimer_confirmation' => 'Do you really want to delete this URL?',

	// T
	'titre_afficher_bots_shortcut_url' => 'Display bots',
	'titre_afficher_logs_shortcut_url' => 'Display logs',
	'titre_ajouter_shortcut_url' => 'Add shortcut URL',
	'titre_csv_export' => 'Data from @date@ extracted on @date_jour@',
	'titre_details_url' => 'Details shortcut URL',
	'titre_export_logs_shortcut_url' => 'Export shortcuts',
	'titre_liste_pays' => 'List of country',
	'titre_modifier_shortcut_url' => 'Edit shortcut URL',
	'titre_page' => 'Title of page',
	'titre_shortcut_url_auteur' => 'List of URLs shortened by author',
	'titre_shortcut_url_enbase' => 'URL information’s already inserted: ',
	'titre_shortcut_url_graph_bot_click' => 'Number of clicks by day for bots',
	'titre_shortcut_url_graph_bots' => 'Shortcut by  bots',
	'titre_shortcut_url_graph_carte' => 'Shortcut by country',
	'titre_shortcut_url_graph_click' => 'Number of clicks by day',
	'titre_shortcut_url_graph_humain_click' => 'Number of clicks by day for humans',
	'titre_shortcut_url_liste' => 'List of shortened links',
	'titre_shortcut_url_liste_log' => 'Shortcut URL Statistics',
	'titre_shortcut_url_liste_logs' => 'Statistics List of shortened links',
	'titre_shortcut_url_liste_logs_bots' => 'Statistic list for bots',
	'titre_shortcut_url_log_detail' => 'Details for shortcut : ',
	'titre_shortcut_url_partage' => 'Share this link',
	'titre_shortcut_urls_logs' => 'Shortcut URL',
	'titre_url' => 'URL',
	'titre_url_clicks' => 'List of connections'
);
