<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/shortcut_url.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_shortcut_title' => 'Ajouter votre propre raccourci',
	'ajouter_shortcut_url' => 'Ajouter une URL',
	'auteur_shortcut_url' => 'Cet URL a été créée par : ',

	// C
	'config_export_ok' => 'L’export des données c’est correctement déroulé',
	'csv_description' => 'description',
	'csv_id' => 'Identifiant',
	'csv_nb_click' => 'Nombre de clics',
	'csv_shortcut' => 'Raccourcis',
	'csv_titre' => 'Titre',
	'csv_url' => 'Url finale',

	// E
	'erreur_url_exist' => 'Cette URL existe déjà',
	'erreur_url_invalide' => 'Veuillez insérer une URL valide',
	'erreur_url_raccourcis_exist' => 'Cette URL raccourcie existe déjà',
	'explication_serveurs_api' => 'Adresses IPs de serveurs ou clients ayant accès à l’API sans identification.',

	// F
	'form_click' => 'Nb clics',
	'form_country_code' => 'Code pays',
	'form_date_connect' => 'Date connexion',
	'form_date_insert' => 'Date insertion',
	'form_date_modif' => 'Date modif',
	'form_description' => 'Description',
	'form_edit' => 'Editer',
	'form_id_shortcut_urls' => 'id',
	'form_ip_address' => 'Adresse ip',
	'form_maj' => 'MAJ',
	'form_nom_pays' => 'Code ISO des pays',
	'form_referrer' => 'Referer',
	'form_titre' => 'Titre',
	'form_url' => 'URL',
	'form_user_agent' => 'User agent',

	// I
	'icone_stats_shortcut' => 'Statistiques',
	'icone_stats_shortcut_url' => 'Retour à la liste statistique',
	'icone_supprimer_shortcut_url' => 'Supprimer l’URL raccourcie',
	'info_1_shortcut_url' => '@nb@ URL raccourcie',
	'info_1_shortcut_url_bot' => 'Clics par les robots',
	'info_1_shortcut_url_humain' => 'Clics par les humains',
	'info_nb_shortcut_url_clicks' => 'Total des clics',
	'info_nb_shortcut_url_stat' => '@nb@ connexion sur l’ensemble des URLs',
	'info_nb_shortcut_url_stats' => '@nb@ connexions sur l’ensemble des URLs',
	'info_nb_shortcut_urls' => '@nb@ URL raccourcies',
	'item_utiliser_shortcut_export' => 'Exporter les statistiques des liens raccourcis',
	'item_utiliser_shortcut_title' => 'Vous pouvez définir le raccourci de votre URL (max @nb@ caractères)',
	'item_utiliser_shortcut_url' => 'Ajouter une URL et puis c’est tout',

	// L
	'label_annee' => 'Sélectionner une année',
	'label_autres' => 'Autres',
	'label_mois' => 'Sélectionner un mois',
	'label_serveurs_api' => 'IPs de serveurs pour l’API',

	// M
	'message_confirmation_shortcut_url' => 'Votre raccourci a été enregistré.',

	// N
	'nb_click' => 'clics',
	'non_communique' => 'nc',

	// P
	'partage_facebook' => 'Partager sur Facebook',
	'partage_googleplus' => 'Partager sur GooglePlus',
	'partage_seenthis' => 'Partager sur Seenthis',
	'partage_twitter' => 'Partager sur Twitter',
	'pas_de_shortcut_url' => 'Pas d’URL raccourcis',
	'pas_de_statistique' => 'Pas de statistique',
	'plugin_d3js_noninstalle' => 'Le plugin d3js n’est pas installé',

	// S
	'shortcut_url' => 'Raccourcis d’URL',
	'shortcut_url_logs' => 'Stats des raccourcis',
	'shortcut_url_logs_export' => 'Export des raccourcis',
	'stats_afficher_bots' => 'Afficher Bots',
	'stats_afficher_graphs_bots' => 'Afficher Graphs Bots',
	'stats_afficher_graphs_logs' => 'Afficher Graphs Logs',
	'stats_afficher_logs' => 'Afficher Logs',
	'supprimer_confirmation' => 'Voulez-vous vraiment supprimer cet URL ?',

	// T
	'titre_afficher_bots_shortcut_url' => 'Afficher les bots',
	'titre_afficher_logs_shortcut_url' => 'Afficher les logs',
	'titre_ajouter_shortcut_url' => 'Ajouter un lien raccourci',
	'titre_configurer_su' => 'Configurer Shortcut URL',
	'titre_csv_export' => 'Données de @date@ extraite le @date_jour@',
	'titre_details_url' => 'Détails de l’URL raccourcie',
	'titre_export_logs_shortcut_url' => 'Exporter des raccourcis',
	'titre_liste_pays' => 'Liste des pays',
	'titre_modifier_shortcut_url' => 'Modifier un lien raccourci',
	'titre_page' => 'Titre de la page',
	'titre_shortcut_url_auteur' => 'Liste des URL raccourcies par auteur',
	'titre_shortcut_url_enbase' => 'Informations sur l’URL déjà insérée : ',
	'titre_shortcut_url_graph_bot_click' => 'Nombre de clics par jour pour les robots',
	'titre_shortcut_url_graph_bots' => 'Raccourcis par bots',
	'titre_shortcut_url_graph_carte' => 'Raccourcis par pays',
	'titre_shortcut_url_graph_click' => 'Nombre de clics par jour',
	'titre_shortcut_url_graph_humain_click' => 'Nombre de clics par jour pour les humains',
	'titre_shortcut_url_liste' => 'Liste des liens raccourcis',
	'titre_shortcut_url_liste_log' => 'Statistique URL raccourcie',
	'titre_shortcut_url_liste_logs' => 'Liste statistique des liens raccourcis',
	'titre_shortcut_url_liste_logs_bots' => 'Liste statistique des bots',
	'titre_shortcut_url_log_detail' => 'Détails pour l’URL : ',
	'titre_shortcut_url_partage' => 'Partager ce lien',
	'titre_shortcut_urls_logs' => 'Raccourcis d’URL',
	'titre_url' => 'URL',
	'titre_url_clicks' => 'Liste des connexions'
);
