<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-shortcut_url?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'shortcut_url_description' => 'Shortcut URL for SPIP', # MODIF
	'shortcut_url_slogan' => 'Shortcut URL for SPIP'
);
